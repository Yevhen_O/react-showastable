import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ShowASTable extends Component {
	render () {
		const { items, headerFields } = this.props;

		return (
			<div>
				<h2>Show as table</h2>
				<table className="custom_table">
					<tbody>
						<tr key='header'>
							{!!items.length && headerFields.map((item) =>
								<th key={item.field}>
									{item.name}
								</th>
							)}
						</tr>
							{items.map((item, index) =>
								<tr key={index}>
									{headerFields.map((i, index) =>
										<td key={i.field}>
											{this.props[`format${i.field}`]
												? this.props[`format${i.field}`](item)
												: item[i.field]
											}
										</td>
									)}
								</tr>
							)}
					</tbody>
				</table>
			</div>		
		)
	}
}

ShowASTable.propTypes = {
	headerFields: PropTypes.array.isRequired,
	items: PropTypes.array.isRequired
}

ShowASTable.defaultProps = {
	headerFields: [],
	items: []
}

export default ShowASTable;


