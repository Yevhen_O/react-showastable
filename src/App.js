import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
// import custom component showAsTable
import ShowAsTable from './components/ShowAsTable';

class App extends Component {

  state = {
    items: [
      {
        id: 1,
        name: 'Custom project',
        author: 'Hannah',
        startDate: '2018-02-13T00:00:00.000Z',
        status: 'In progress'
      },
      {
        id: 2,
        name: 'Analytics',
        author: 'Samantha',
        startDate: '2018-02-10T00:00:00.000Z',
        status: 'In progress'
      },
      {
        id: 3,
        name: 'Social media',
        author: 'Serhii',
        startDate: '2018-02-23T00:00:00.000Z',
        status: 'In progress'
      },
      {
        id: 4,
        name: 'Learning React',
        author: 'Andrew',
        startDate: '2017-12-13T00:00:00.000Z',
        status: 'Completed'
      },
      {
        id: 5,
        name: 'JavaScript testing',
        author: 'Andrew',
        startDate: '2018-01-03T00:00:00.000Z',
        status: 'In progress'
      },
      {
        id: 6,
        name: 'Environment problems',
        author: 'Samantha',
        startDate: '2017-11-22T00:00:00.000Z',
        status: 'Completed'
      },
      {
        id: 7,
        name: 'Public relations',
        author: 'Hannah',
        startDate: '2018-02-12T00:00:00.000Z',
        status: 'In progress'
      },
      {
        id: 8,
        name: 'Social programs',
        author: 'Hannah',
        startDate: '2018-01-31T00:00:00.000Z',
        status: 'Suspended'
      },
      {
        id: 9,
        name: 'Redux state management',
        author: 'Serhii',
        startDate: '2017-10-12T00:00:00.000Z',
        status: 'Completed'
      },
      {
        id: 10,
        name: 'Soft skills training',
        author: 'Andrew',
        startDate: '2017-05-03T00:00:00.000Z',
        status: 'Suspended'
      }
    ],
    headerFields: [
      {
        field: 'name',
        name: 'Project Name'
      },
      {
        field: 'startDate',
        name: 'Start Date'
      },
      {
        field: 'author',
        name: 'Author'
      },
      {
        field: 'status',
        name: 'Status'
      }
    ]
  }

  formatStartDate = ({ startDate }) => {
		const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		let m = new Date(startDate);
		let dateString = ('0' + m.getUTCDate()).substr(-2) +"  "+ months[m.getUTCMonth()] +" "+ m.getUTCFullYear();
		return dateString;
	}

	formatStatus = ({ status }) =>
		<span className={status.toLowerCase().replace(' ', '_')}>{status}</span>;

  render() {
    const { items, headerFields } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="App-intro">
          <ShowAsTable
            items={items}
            headerFields={headerFields}
            formatstatus={this.formatStatus}
            formatstartDate={this.formatStartDate}
          />
        </div>
      </div>
    );
  }
}

export default App;
